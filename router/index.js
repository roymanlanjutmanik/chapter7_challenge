const express = require("express");
const router = express.Router();
const { restrict } = require("../middlewares/restrict");

const authCtrl = require("../controllers/auth.controller");
const homeCtrl = require("../controllers/home.controller");
//Home Page
router.get("/", homeCtrl.showIndexPage);

// //Register Page
router.get("/register", authCtrl.showRegisterPage);
router.post("/register", authCtrl.register);
router.get("/register-success", authCtrl.showRegisterSuccess);

//Login Page
router.get("/login", authCtrl.showLoginPage);
router.post("/login", authCtrl.login);
router.get("/error-login", authCtrl.showErrorLogin);

// rps game
router.get("/rps", authCtrl.rps)

//WhoAmI Page
router.get("/whoami", authCtrl.whoAmI);

//Error handler
router.get((req, res, next) => {
  const error = new Error("NOT FOUND");
  error.status = 404;
  next(error);
});

module.exports = router;
