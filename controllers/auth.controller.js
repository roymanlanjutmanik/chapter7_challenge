const { User } = require("../models");
const user = require("../models/user");
const bcrypt = require("bcrypt");
const passport = require("../lib/passport");

const showRegisterPage = (req, res) => {
  res.render("register");
};

const showRegisterSuccess = (req, res) => {
  res.render("register-success");
};

const showErrorLogin = (req, res) => {
  res.render("error-login");
};

const register = (req, res, next) => {
  const { username, email, password } = req.body;
  const encryptedPassword = bcrypt.hashSync(password, 10);
  User.create({
    username,
    email,
    password: encryptedPassword,
  })
    .then(() => {
      res.redirect("/register-success");
    })
    .catch((error) => next(error.messagge));
};

const showLoginPage = (req, res) => {
  res.render("login");
};
const login = passport.authenticate("local", {
  successRedirect: "/rps",
  failureRedirect: "/error-login",
  failureFlash: true,
  successFlash: true,
});

const whoAmI = (req, res) => {
  res.render("profile", req.user.dataValues);
};

const rps = (req, res) => {
  res.render("rps");
};

module.exports = {
  register,
  showRegisterPage,
  showLoginPage,
  showRegisterSuccess,
  showErrorLogin,
  login,
  whoAmI,
  rps,
};
