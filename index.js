// imports
const express = require("express");
const session = require("express-session");
const flash = require("express-flash");
const morgan = require("morgan");
const passport = require("./lib/passport");

const app = express();
const PORT = process.env.PORT || 3000;

// Middleware
app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Middleware Session
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use(
  session({
    secret: "Binar Secret",
    resave: false,
    saveUninitialized: false,
  })
);

// Routing
const router = require("./router");
app.use(router);

// View Engine
app.set("view engine", "ejs");

//static file
app.use(express.static("public"));
app.use("/css", express.static(__dirname + "/public/assets/css"));
app.use("/js", express.static(__dirname + "/public/js"));
app.use("/img", express.static(__dirname + "/public/assets/img"));

app.listen(PORT, () => {
  console.log(`Server berjalan di port ${PORT}`);
});
